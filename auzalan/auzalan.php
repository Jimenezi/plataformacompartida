<?php
/**
 * Plugin Name: AUZALAN y FORMACIÓN AUZALAN
 * Plugin URI: https://www.auzalan.net
 * Description: conexión Auzalan.  proporcion de imagenes: 6x4 ejemplo: 600px X 400px  o  300px X 200px
 * Version: 22.01.31
 * Author: Angel Alcaide
 * Author URI: http://www.i-habite.com
 */ 
define('__AUZ_VERSION__','07.04.2022');   
/*  ------------------------------------------------------------------------------------------------------------*/
define('__CLAVE_USUARIO__','yyyy'); 
define('__AUZALAN_URL__',"xxxx");  
define('__AUZALAN_POST_VIEWER_PAGE__',__AUZALAN_URL__."noticias-cms/");
define('__FORMACION_AUZALAN_POST_VIEWER_PAGE__',__AUZALAN_URL__."formacion-compartida-detalle/");
define('__AUZALAN_ACTIVATIE_POST_VIEWER_PAGE__',__AUZALAN_URL__."formacion-activatie-detalle/");
define('__AUZALAN_FORMACION_TEXTO_NoResultados__',"<p>En estos momentos no hay actividades en periodo de inscripción.</p>");

/*  ------------------------------------------------------------------------------------------------------------*/
define('__AUZALAN_DIR__',__AUZALAN_URL__."wp-content/plugins/auzalan/");
/*  ------------------------------------------------------------------------------------------------------------*/
define('__MAX_LENGTH_caracteresTitulo_estilo7__',85);	//Usado estilo 7
define('__MAX_LENGTH__',170);	//Usado en Ver más, limite de caracteres a mostrar
define('__AUZALAN_MAX_NODOS__',50);
define('__AUZALAN_ID__','112');

// AUZALAN
define('__AUZALAN_WEB__','https://www.auzalan.net/views/WebServices/wsread_e.aspx?');
define('__AUZALAN_WEB_DIRECTORIO__','https://www.auzalan.net/images/cms/');
// FORMACION AUZALAN
define('__FORMACION_WEB__','https://formacion.auzalan.net/project/WebServices/wsread_e.aspx?');


// FORMACION activatie
define('__FORMACION_ACTIVATIE__','https://www.activatie.org/web/formacion_json.php');

// LOAD TEMPLATES
require_once('templates/auzalan-post-viewer.php');
require_once('templates/auzalan-template-old.php');
require_once('templates/auzalan-template-arbol.php');
require_once('templates/auzalan-template-cajas.php');
require_once('templates/auzalan-template-galeriav1.php');
require_once('templates/auzalan-template-galeriav1-estilo2.php');
require_once('templates/auzalan-template-galeriav1-estilo3.php');
require_once('templates/auzalan-template-galeriav1-estilo3-2.php');
require_once('templates/auzalan-template-galeriav1-estilo3-3.php');
require_once('templates/auzalan-template-galeriav1-estilogippre.php');  // estilo GIPUZKOA PRENSA
require_once('templates/auzalan-template-galeriav1-estilo4.php');
require_once('templates/auzalan-template-galeriav1-estilo5.php');
require_once('templates/auzalan-template-galeriav1-estilo6.php');
require_once('templates/auzalan-template-galeriav1-estilo7.php');     // estilos new Albacete
require_once('templates/auzalan-template-galeriav1-estilo7-detalle.php');  // estilos new Albacete
require_once('templates/auzalan-template-galeriav1-estilo8.php');  // estilos new Albacete portada


require_once('templates/auzalan-formacion-template-galeriav1-estilo1.php');
require_once('templates/auzalan-formacion-template-galeriav1-estilo2.php');
require_once('templates/auzalan-formacion-template-galeriav1-estilo3.php');
require_once('templates/auzalan-formacion-template-galeriav1-estilo4.php');
require_once('templates/auzalan-formacion-template-galeriav1-estilo5.php');
require_once('templates/auzalan-formacion-template-galeriav1-estilogippre.php');
require_once('templates/auzalan-formacion-template-galeriav1-estilo7.php');  // estilos new Albacete
require_once('templates/auzalan-formacion-template-galeriav1-estilo7-detalle.php');  
// estilos activatie
require_once('templates/auzalan-activatie-estilo1.php');
require_once('templates/auzalan-activatie-estilo3.php');
require_once('templates/auzalan-activatie-estilo4.php');   // detalle del curso
require_once('templates/auzalan-activatie-estilo7.php');   // Vista Albacete
require_once('templates/auzalan-activatie-estilo7-detalle.php');   // Vista Albacete

add_shortcode('plg_auzalan', 'f_plg_auzalan');
add_shortcode('plg_auzalan_cajas', 'f_plg_auzalan_cajas');
add_shortcode('plg_auzalan_arbol', 'f_plug_auzalan_arbol');
add_shortcode('plg_auzalan_arbol_solo_nodos', 'f_plug_auzalan_arbol');
add_shortcode('plg_auzalan_arbol_no_breadcrumb', 'f_plug_auzalan_arbol_no_breadcrumb');
add_shortcode('plg_auzalan_post_viewer', 'f_plg_auzalan_post_viewer');
add_shortcode('plg_auzalan_detalle_nodo', 'f_plg_auzalan_detalle_nodo');
add_shortcode('plg_auzalan_post_viewer_form', 'f_plg_auzalan_post_viewer_form');
add_shortcode('plg_auzalan_galeria', 'f_plg_auzalan_galeria');
add_shortcode('plg_auzalan_activatie_detalle', 'f_plg_auzalan_activatie_detalle');

/**
* Formato clasico V2
* Devuelve los post de auzalan en formato clásico
* @param array Atributos pasados desde el plugin
* @return string Post en formato HTML
*/
function f_plg_auzalan_activatie_detalle($atts){
		
		$post_id = htmlspecialchars($_GET["postid"]);
		if (!empty($post_id)) {
			$posts = get_activatie_posts($atts);
			
			if (!empty($posts)) {			 
				
				$estilo='estilo1';
				if (!empty($atts["estilo"])) {
					$estilo=$atts["estilo"];
				}			
				switch ($estilo) {
					case "estilo1": // por defecto
					return auzalan_activatie_estilo4($posts, $atts, $post_id );
					case "estilo7":  // new albacete
					return auzalan_activatie_estilo7_detalle($posts, $atts, $post_id );
				}			
			}			
		}else{
			return "Es necesario un parámetro";
		}	
		
}

function f_plg_auzalan_galeria($atts){
		
		   $idioma='';
		   $bbdd='auzalan';
		   if (!empty($atts["bdd"])) {
					$bbdd=$atts["bdd"];
		   }
		   $version='estilo1';
		   if (!empty($atts["estilo"])) {
					$version=$atts["estilo"];
	       }
		   if (!empty($atts["idioma"])) {
					$idioma=$atts["idioma"];
		   }
			if ($bbdd=='auzalan'){
		 
				$posts = get_auzalan_posts($atts,__AUZALAN_WEB__);	
				if (!empty($posts)) {				 		
				switch ($version) {
					case "estilo1":    // por defecto 
									return  auzalan_template_galeria_v1($posts, $atts);
					case "estilo2":    // por defecto 
									return  auzalan_template_galeria_v1_estilo2($posts, $atts);		
					case "estilo3":    // por defecto 
									return  auzalan_template_galeria_v1_estilo3($posts, $atts);		
					case "estilo4":    // por defecto 
									return  auzalan_template_galeria_v1_estilo4($posts, $atts);	
					case "estilo5":    // por defecto 
									return  auzalan_template_galeria_v1_estilo5($posts, $atts);	
					case "estilo6":    // por defecto 
									return  auzalan_template_galeria_v1_estilo6($posts, $atts);	
					case "estilo3_2":    // por defecto 
									return  auzalan_template_galeria_v1_estilo3_2($posts, $atts);
					case "estilo3_3":    // por defecto 
									return  auzalan_template_galeria_v1_estilo3_3($posts, $atts);
					case "estilogippre":    // gipuzkoa PRENSA 
									return  auzalan_template_galeria_v1_estilogippre($posts, $atts);
					case "estilo7":    // nueva página Albacete
									return  auzalan_template_galeria_v1_estilo7($posts, $atts);	
					case "estilo7p":    // nueva página Albacete PORTADA
									return  auzalan_template_galeria_v1_estilo7p($posts, $atts);	
					case "estilo7columnas":    // nueva página Albacete PORTADA
									return  auzalan_template_galeria_v1_estilo7_3variascolumnas($posts, $atts);	
					case "estilo7columnassinimagen":    // nueva página Albacete PORTADA
									return  auzalan_template_galeria_v1_estilo7_3variascolumnas_sinImagen($posts, $atts);	
					case "estilo8":    // PORTADA
									return  auzalan_template_galeria_v1_estilo8($posts, $atts);	
				}}
			}
			if ($bbdd=='formacion'){
						 $devuelve="-3-";
				$posts = get_auzalan_posts($atts,__FORMACION_WEB__);	
				if (!empty($posts)) {
					switch ($version) {
						case "estilo1":    // varias columnas
										return  auzalan_formacion_template_galeria_v1_estilo1($posts, $atts);
						case "estilo2":    // varias columnas
										return  auzalan_formacion_template_galeria_v1_estilo2($posts, $atts);		
						case "estilo3":    // varias columnas
										return  auzalan_formacion_template_galeria_v1_estilo3($posts, $atts);		
						case "estilo4":    // A UNA COLUMNA, MUESTRA TODO EL CONTENIDO SIN VER MAS
										return  auzalan_formacion_template_galeria_v1_estilo4($posts, $atts,"");  // agregado campo idioma	
						case "estilo5":    // IGUAL AL ESTILO 4 - MISMA HOJA ESTILOS- PERO ES CORTO, SOLO MUESTRA DESCRIPCÍÓN Y UN VER MÁS.
										return  auzalan_formacion_template_galeria_v1_estilo5($posts, $atts);	
						case "estilo7":    // new Albacete
										return  auzalan_formacion_template_galeria_v1_estilo7($posts, $atts);	
						case "estilo7p":    // nueva página Albacete PORTADA
									return  auzalan_formacion_template_galeria_v1_estilo7p($posts, $atts);
						case "estilogippre":    //(2020-09-21) IGUAL AL ESTILO 5 - MISMA HOJA ESTILOS- PERO VER MÁS ABRE PDF.
										
										/*if ($idioma=='idioma2'){
											return  auzalan_formacion_template_galeria_v1_estilogippre_idioma2($posts, $atts);										
										}else{*/
											return  auzalan_formacion_template_galeria_v1_estilogippre($posts, $atts);										
										 
					}
				}else{
					return '<!-- AUZALAN VERSION: ' . __AUZ_VERSION__ .' --><p>' . __AUZALAN_FORMACION_TEXTO_NoResultados__ . '</p>';
				}
			}
			if ($bbdd=='activatie'){
				 $posts = get_activatie_posts($atts);
				 
			 	if (!empty($posts)) {				 		
				switch ($version) {
						case "estilo1":  
						return  auzalan_activatie_estilo1($posts, $atts);	
						case "estilo2":  
						return  auzalan_activatie_estilo1($posts, $atts);   // ojo no existe estilo 2	
						case "estilo3":  
						return  auzalan_activatie_estilo3($posts, $atts);
						case "estilo7":  
						return  auzalan_activatie_estilo7($posts, $atts);							
					}
				}else{
					return '<!-- AUZALAN VERSION: ' . __AUZ_VERSION__ .' --><p>' . __AUZALAN_FORMACION_TEXTO_NoResultados__ . '.</p>';
				} 
			}
				
}

/**
* Devuelve los post de auzalan en formato clásico
* @param array Atributos pasados desde el plugin
* @return string Post en formato HTML
*/
function f_plg_auzalan( $atts ) {		
	$posts = get_auzalan_posts($atts,__AUZALAN_WEB__);	
	return  template_old($posts, $atts);
}


/**
* Devuelve los post de auzalan en formato arbol navarro
* @param array Atributos pasados desde el plugin
* @return string Post en formato HTML
*/
function f_plug_auzalan_arbol( $atts ){
	$path = htmlspecialchars($_GET["path"]);
	if($path != null){
		// Obtener los post de auzalan de Path
		$posts = get_auzalan_arbol($path,$atts);
	}else{
		$posts = get_auzalan_arbol("",$atts);
	}
	return auzalan_template_arbol($posts);	
}

function f_plug_auzalan_arbol_no_breadcrumb( $atts ){
	$path = htmlspecialchars($_GET["path"]);
	if($path != null){
		// Obtener los post de auzalan de Path
		$posts = get_auzalan_arbol($path,$atts);
	}else{
		$posts = get_auzalan_arbol("",$atts);
	}
	return auzalan_template_arbol_no_breadcrumb($posts);	
}


function f_plg_auzalan_cajas( $atts ) {		
	$posts = get_auzalan_posts($atts,__AUZALAN_WEB__);	
	if (empty($posts)) {
		return ' ';
		}else{
		return  template_cajas($posts, $atts);
	}
	
}



function f_plg_auzalan_arbol_solo_nodos($atts){
	$posts = get_auzalan_posts($atts,__AUZALAN_WEB__);	
	return auzalan_template_arbol_solo_posts($posts);	
}



function f_plg_auzalan_post_viewer_2($atts){
	$post_id = htmlspecialchars($_GET["postid"]);
	$musaat = htmlspecialchars($_GET["musaat"]);
	if($musaat == null){
		$musaat = false;
	}
	$posts = get_auzalan_post($post_id,__AUZALAN_WEB__,$musaat);
	if (empty($posts)) {
	return ' ';
		}else{
		
		$estilo='estilo1';
		   if (!empty($atts["estilo"])) {
					$estilo=$atts["estilo"];
	       }
		
		switch ($estilo) {
			case "estilo1": // por defecto
			return auzalan_template_galeria_v1_estilo4($posts, $atts);
			case "estilo7":  // new albacete
			return auzalan_template_galeria_v1_estilo7_detalle($posts, $atts);
		}
	}
}

function f_plg_auzalan_detalle_nodo($atts){
	$post_id = htmlspecialchars($_GET["postid"]);
	 
	$posts = get_auzalan_post($post_id,__AUZALAN_WEB__,false);
	if (empty($posts)) {
			return ' ';
		}else{
			 
			return auzalan_template_galeria_v1_estilo7_3variascolumnas_detalle($posts, $atts);
		}
	 
} 
function f_plg_auzalan_post_viewer($atts){
	$post_id = htmlspecialchars($_GET["postid"]);
	$musaat = htmlspecialchars($_GET["musaat"]);
	if($musaat == null){
		$musaat = false;
	}
	$posts = get_auzalan_post($post_id,__AUZALAN_WEB__,$musaat);
	if (empty($posts)) {
	return ' ';
		}else{
		
		$estilo='estilo1';
		   if (!empty($atts["estilo"])) {
					$estilo=$atts["estilo"];
	       }
		
		switch ($estilo) {
			case "estilo1": // por defecto
			return auzalan_template_galeria_v1_estilo4($posts, $atts);
			case "estilo7":  // new albacete
			return auzalan_template_galeria_v1_estilo7_detalle($posts, $atts);
		}
	}
}
function f_plg_auzalan_post_viewer_form($atts){
	$post_id = htmlspecialchars($_GET["postid"]);
	$idioma ="";
	if (isset($_GET['idioma'])){
		$idioma = htmlspecialchars($_GET["idioma"]);
	} 
	$posts = get_auzalan_post_formacion($post_id,$idioma,__FORMACION_WEB__);
	if (empty($posts)) {
		
				return ' ';
		}else{
		//return post_viewer($posts);
		
		$estilo='estilo1';
		if (!empty($atts["estilo"])) {
					$estilo=$atts["estilo"];
	    }
		
		switch ($estilo) {
			case "estilo1": // por defecto
			return auzalan_formacion_template_galeria_v1_estilo4($posts, $atts,$idioma);
			case "estilo7":  // new albacete
			return auzalan_formacion_template_galeria_v1_estilo7_detalle($posts, $atts,$idioma);
		}
		
		
	}
}
/*****************************************************
**														
**		Conexión con Auzalan						
**													
******************************************************/
function get_auzalan_post_formacion($post_id,$idioma,$BASEURL){

	$url_data = array(
	'param2' => __CLAVE_USUARIO__,
	'param4' => $post_id	
	);
	  if ($idioma!=""){
			$url_data = array(
			'param2' => __CLAVE_USUARIO__,
			'param4' => $post_id,
			'idioma' => $idioma
	);}
	 
	
	
	$url = $BASEURL . http_build_query($url_data);
	$url = quitar_caracteres_raros($url); 
	$datos = wp_remote_get($url, array('timeout' => 6));
	//así si funciona --> $datos = wp_remote_get('https://formacion.auzalan.net/project/WebServices/wsread_e.aspx?param2=HMsQsEyj&param4=112');
	
	
	if(is_wp_error($datos)){
		return "<!--002--- error en auzalan, timeout  -->";
	} 
	else {
		
		$array= json_decode($datos['body'], true);
		
		if(count($array) <= 0){
			return "";
		} else {
		
			return $array;			
		}		
	}
}
function get_auzalan_post($post_id,$BASEURL,$musaat){
	$clave_usuario = __CLAVE_USUARIO__;
	if($musaat){
		$clave_usuario = 'J6936566A';  //MUSAAT
	}
	
	$url_data = array(
	'param1' => __AUZALAN_ID__,
	'param2' => $clave_usuario,
	'param7' => $post_id
	);
	
	$url = $BASEURL . http_build_query($url_data);
	$url = quitar_caracteres_raros($url); 
	$datos = wp_remote_get($url, array('timeout' => 6));
	
	if(is_wp_error($datos)){
		return "<!--002--- error en auzalan, timeout  -->";
	} 
	else {
		
		$array= json_decode($datos['body'], true);
		
		if(count($array) <= 0){
			return "";
		} else {
			return $array;			
		}		
	}
}

/**
* Devuelve los post de auzalan en un arrat
* @param array Atributos pasados desde el plugin que filtran los posts
* @return array<std_class> Array de psots
*/
function get_auzalan_posts($atts,$BASEURL){
	$atts = shortcode_atts(
		array(
			'id' => __AUZALAN_ID__,
			'num_nodos' => '',
			'buscar' => '',
			'cat_hijo' => '',
			'cat_padre' => '',
			'estilo' => 'columnas_1',
			'id_nodo' => '', 
			'idioma' => '' 
		), $atts, 'auzalan2' );
	
	$claveUsuario = __CLAVE_USUARIO__;
	if ($atts['cat_hijo'] == '172'){
		$claveUsuario = 'J6936566A';    // MUSAAT
	}
	// OJO: Esta funcion se usa para formación y auzalan, monta el GET URL DE AMBOS.
	// EN AUZALAN  --> EL CAMPO IDIOMA ES param9
	// EN FORMACIÓN--> EL CAMPO IDIOMA ES idioma
	$url_data = array(
	'param1' => $atts["id"],
	'param2' => $claveUsuario,
	'param3' => $atts["num_nodos"],
	'param4' => $atts["buscar"],
	'param5' => $atts["cat_hijo"],
	'param6' => $atts["cat_padre"],
	'param7' => $atts["id_nodo"],
	'param9' => $atts["idioma"],
	'idioma' => $atts["idioma"],
	'orden' => $atts["orden"]
	);
	
	$url = $BASEURL . http_build_query($url_data);
	$url = quitar_caracteres_raros($url); 
	
	$datos = wp_remote_get($url, array('timeout' => 6));
	if(is_wp_error($datos)){
		return "<!--002--- error en auzalan, timeout  -->";
	}else {
		$array= json_decode($datos['body'], true);
		if(count($array) <= 0){
			return "";
		} else {
			/*COMPRUEBO SI HAY DATOS*/			
			return $array;		
					
		}		
	}
}

/**
 * Obtiene el arbol de la categoría dada
 * @param string categoria
 * @return std_class Decoded json
 */
function get_auzalan_arbol($categoria,$atts){
	$atts = shortcode_atts(
		array(
			'path'=> "0",
			'buscar' => ''
		), $atts, 'auzalan2' );
		
	
	if( $categoria == null || $categoria == ""){
		$categoria = $atts['path'];
	}
	$url_data = array(
	'param1' => __AUZALAN_ID__,
	'param2' => __CLAVE_USUARIO__,
	'param4' => $atts["buscar"],
	'arbol' => $categoria
	);
	
	$url = __AUZALAN_WEB__ . http_build_query($url_data);
	
	$url = quitar_caracteres_raros($url); 
	$datos = wp_remote_get($url, array('timeout' => 6));
	if(is_wp_error($datos)){
		return "<!--002--- error en auzalan, timeout  -->";
	} 
	else {
		$json= json_decode($datos['body'], true);		
		return $json;	
	}
}


function get_activatie_posts($atts){
	$atts = shortcode_atts(
		array(
			'num_nodos' => '',
			'estilo' => 'columnas_1',
			'id_nodo' => '' // Para mostrar solamente un único nodo
		), $atts, 'activatie' );
	 
	
	$url = __FORMACION_ACTIVATIE__;
	 
	
	$datos = wp_remote_get($url, array('timeout' => 6));
	if(is_wp_error($datos)){
		return "<!--004--- error en auzalan, timeout  -->";
	}else {
	
	// OJO ACTIVATIE DEVUELVE: {"curso":[{...},{...}]}
	
		$array= json_decode($datos['body'], true);
		if(count($array) <= 0){
			return "<!--003--- error en auzalan, devolución   -->";
		} else {
			/*COMPRUEBO SI HAY DATOS*/			
		 	return $array;		
			 
		}		
	}
}


/*****************************************************
**														
**		Funciones Auxiliares						
**													
******************************************************/

/**
* Quita caracteres innecesarios de las URLS
* @param string url
* @return string url codificada
*/
function quitar_caracteres_raros($cadena){
$caracteres = 'À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö Ø Ù Ú Û Ü Ý Þ ß à á â ã ä å æ ç è é ê ë ì í î ï ð ñ ò ó ô õ ö ø ù ú û ü ý þ ÿ ¡ ¢ £ ¤ ¥ ¦ § ¨ © ª « ¬ ® ¯ ° ± ² ³´ µ ¶ · ¸ ¹ º » ¼ ½ ¾ ¿ × ÷ ” \’ "';
$caracteres = explode(' ',$caracteres);
$cadena = str_replace($caracteres,'',$cadena);
return $cadena;
}



