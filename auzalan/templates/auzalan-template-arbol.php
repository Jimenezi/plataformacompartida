<?php

/*
* TEMPLATES
*/


/**
*	Template arbol con breadcrumb y carpetas
*/
function auzalan_template_arbol($decoded_json) {

        //Construir el arbol (BREADCRUMBS)
        $breadcrumb = get_breadcrumb($decoded_json["ruta"]);

        //Subcarpetas
        $subcarpetas = get_subcarpetas($decoded_json["subcarpetas"]);

        //Nodos        
        $nodos = get_nodos($decoded_json["nodos"]);
    
	if($nodos=="" &&  $subcarpetas ==""){
		$nodos = "<p>No hay contenido para mostrar</p>";
	}

     $estilo = style();
    $script = get_script();
  //  $bootstrap = load_bootstrap();
    return ('<div id="auza_posts">'. $bootstrap . $estilo . $breadcrumb . $subcarpetas . '<div style="margin-top:5px">' . $nodos . $script . '</div>');
}

function auzalan_template_arbol_no_breadcrumb($decoded_json) {

        //Construir el arbol (BREADCRUMBS)
      //  $breadcrumb = get_breadcrumb($decoded_json["ruta"]);

        //Subcarpetas
        $subcarpetas = get_subcarpetas($decoded_json["subcarpetas"]);

        //Nodos        
        $nodos = get_nodos($decoded_json["nodos"]);
    
	if($nodos=="" &&  $subcarpetas ==""){
		$nodos = "<p>No hay contenido para mostrar</p>";
	}

     $estilo = style();
    $script = get_script();
  //  $bootstrap = load_bootstrap();
    return ('<div id="auza_posts">'. $bootstrap . $estilo . $breadcrumb . $subcarpetas . '<div style="margin-top:5px">' . $nodos . $script . '</div>');
}


/**
*	Template arbol pasando solo un array de posts
*/
function auzalan_template_arbol_solo_posts($posts){
	$nodos = get_nodos($posts);
	return ('<div id="auza_posts">'. $bootstrap . style(). '<div style="margin-top:5px">' . $nodos . $script . '</div>');
}









/**
 * Crea el html dado el json con la ruta
 * @param array $ruta_array
 * @return string Componente html con la ruta y los enlaces
 */
function get_breadcrumb($ruta_array) {
	if(!$ruta_array){
		return null;
	}
	$actual_link = __AUZALAN_URL__."$_SERVER[REQUEST_URI]";
	$no_params_url = explode("?",$actual_link)[0];
	
    $breadcrumb = "<p>";
	
    foreach ($ruta_array as $i => $nodo) {        
        $breadcrumb .= "<a href=\"" . $no_params_url.'?path='. $nodo["id"] . "\">" . $nodo["nombre"] . "</a>" . " > ";
    }
    $breadcrumb .= "</p>";
    return $breadcrumb;
}

/**
 * Crea el HTML para mostrar las subcarpetas
 * @param array $carpetas_array
 * @return string Div HTML con las carpetas
 */
function get_subcarpetas($carpetas_array) {
	if(!$carpetas_array){
		return "";
	}
	if(count($carpetas_array)==0){
		return "";
	}
    $subcarpetas = '<div class="nodo-table col-md-12 col-sm-12">';
    $contenedor = '<div>';
	$actual_link = __AUZALAN_URL__."$_SERVER[REQUEST_URI]";
	$no_params_url = explode("?",$actual_link)[0];
    foreach ($carpetas_array as $i => $nodo) {
		$icon = '<img src="'.__AUZALAN_WEB_DIRECTORIO__.'carpeta_roja.png" class="nodo-icon" alt="imagen" >'; 
        $contenedor .= '<div class="carpeta nodo-table col-xs-12 col-md-12 col-sm-12 col-lg-12">';
		$contenedor .= "<a href=\"" . $no_params_url .'?path='. $nodo["id"]. "\">";
		$contenedor .= '
			<table class="nodo-enlace-table">
				<tr>
				  <td class="nodo-enlace-icon" valign="center">'.$icon.'</td>
				  <td class="nodo-enlace-title" valign="center"><h5 class="nodo-enlace-title">' . $nodo["nombre"] . ' </h5></td>
				</tr>
			</table>';				
        $contenedor .= "</a>";
        $contenedor .= '</div>';
    }
    $contenedor .= "</div>";
    $subcarpetas .= $contenedor . "</div>";
    return $subcarpetas;
}

function get_nodos($array) {
	if(!$array){
		$nodos = "";
	}
	
	if(count($array) == 0){
		$nodos = "";
	}else{		
		 $nodos = "";
		foreach ($array as $i => $nodo) {
			$nodos .= nodo_titulo_enlace($nodo);
			$nodos = '<div class="col-md-12 col-sm-12 nodo-table">' . $nodos . '</div>';
		}
	}  
   
    return $nodos;
}

function nodo_titulo_enlace($auzalan_nodo) {
$nodo = '<!-- sin conexion --> ';
if (!empty($auzalan_nodo['id'])) {

    $icon = '56'; // ID Tablón de anuncios (cambia si [CategoriaPadre] != NULL)
    if ($auzalan_nodo['CategoriaPadre'] != Null) {
        $icon = $auzalan_nodo['CategoriaPadre'];
    }
    $icon = '<img src="'.__AUZALAN_WEB_DIRECTORIO__.'' . $icon . '.png" class="nodo-icon" alt="imagen" >'; //CADENA 6 -> Imagen de la categoríawidth="40px"
    $nodo = '<div class="nodo_titulo_enlace col-xs-12 col-md-12 col-sm-12">';

	if($auzalan_nodo['Url'] != null && $auzalan_nodo['Url'] != ""){
		$enlace= ' <a href="'.$auzalan_nodo['Url'].'" target="_blank" >';		
	}
	else{
		$enlace= ' <a href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $auzalan_nodo['id'].'">';
	}	
	$nodo .= '
	<table class="nodo-enlace-table">
		<tr>
		  <td class="nodo-enlace-icon" valign="center"  >' .$enlace.$icon.'</a></td>
		  <td class="nodo-enlace-title" valign="center">' .$enlace. $auzalan_nodo['Titulo'] . ' </a></td>
		</tr>
	</table>';
    $nodo .= '</div>';
}
    return $nodo;
}

//-------------  hojas de estilos-----------------------------------------------
function style() {
    return    '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/template-arbol.css" type="text/css"><link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/style.css" type="text/css">';
}

//------------ Scripts ---------------------------------------------------------
function get_script() {
    return '
	<script>
jQuery(document).ready(function() {
  	
    var max = ' . $maxLength . ';
    var cadena, subcadena;

    jQuery(".subcadena").each(function() {
        cadena = jQuery(this).parent().children(".oculto").html();
        if(cadena.length > max) {
        	subcadena = jQuery(this).parent().children(".oculto").html().substring(0, max);
    		jQuery(this).html(subcadena + " ...");
        } else {
			jQuery(this).parent().children(".oculto").css("display","");
			jQuery(this).parent().children(".abrir").css("display","none");
        }
	});

    jQuery(".abrir").click(function(e){
    	jQuery(this).parent().children(".oculto").toggle(0, function(){
        	if(jQuery(this).parent().children(".oculto").css("display")=="none"){
        		subcadena = jQuery(this).parent().children(".oculto").html().substring(0, max);
    			jQuery(this).parent().children(".subcadena").html(subcadena + " ...");
            	jQuery(this).parent().children(".abrir").html("Ver más");
            } else {
            	jQuery(this).parent().children(".subcadena").html("");
            	jQuery(this).parent().children(".abrir").html("Ver menos");
            }
			jQuery("html, body").animate({scrollTop: jQuery(this).parent().children(".titulo").offset().top - 130},300);
		});
        e.preventDefault();	
	});
});
</script>';
}

function load_bootstrap() {
    return '
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>';
}
