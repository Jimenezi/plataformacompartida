<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 				[plg_auzalan_galeria bdd="formacion" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*        formacion                             ESTILO 4        */
function auzalan_formacion_template_galeria_v1_estilo4( $array, $atts,$idioma ) {
 
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	
	
	// ----- idioma ----------------- ----------------- ----------------- -----------------
	
	$lng_Objetivos="Objetivos";
	$lng_Programa="Programa";
	$lng_Requisitos="Requisitos";
	$lng_TipoActividad="Tipo de actividad";
	$lng_AreaTematica="Area temática";
	$lng_LugarCelebracion="Lugar de celebración";
	$lng_HorasLectivas="Horas lectivas";
	$lng_FlimiteInscripcion="Fecha límite de Inscripción";
	$lng_FlimiteInicio="Fecha inicio del curso";
	$lng_FlimiteFin="Fecha fin del curso";
	$lng_Calendario="Calendario";
	$lng_CursoSubvencionado ="Curso Subvencionado";
	$lng_DescargarAdjunto ="descargar adjunto";
	$lng_verMas="Ver más";
	$lng_verMenos="Ver menos";
	$lng_verPublicacion="Ver publicación";
	$lng_verVideo="Ver vídeo";
	$lng_inscripcion="inscripcion";
	
		if ($idioma == 'idioma2')  {	
				 $lng_Objetivos="Helburuak";
				 $lng_Programa="Programa";
				 $lng_Requisitos="Baldintzak";
				 $lng_TipoActividad="Jarduera mota";
				 $lng_AreaTematica="Gaikako arloa";
				 $lng_LugarCelebracion="Ospakizun lekua";
				 $lng_HorasLectivas="Irakaskuntza orduak";
				 $lng_FlimiteInscripcion="Izena emateko epea";
				 $lng_FlimiteInicio="Ikastaroaren hasiera data";
				 $lng_FlimiteFin="Ikastaroaren amaiera data";
				 $lng_CursoSubvencionado ="Diruz lagundutako ikastaroa";
				 $lng_Calendario="Egutegia";
				 $lng_DescargarAdjunto ="eranskina deskargatu";
				 $lng_verMas="Gehiago ikusi";
				 $lng_verMenos="Gutxiago ikusi";
				 $lng_verPublicacion="Ikusi argitalpena";
				 $lng_verVideo="Ikusi bideoa";
				 $lng_inscripcion="inskripzioa";
		}
	// ----- fin idioma ----------------- ----------------- ----------------- ----------------- ----------------- -----------------
	
	
	
	
	for($i=0;$i<count($array);$i++){
     $mostrar=1;
	 if (!empty($array[$i]['idPb'])) {
	/*
		ERROR SI INCLUYO ATTS...
	*/
	 if ( $mostrar==1){
		$imagen='';
		$imagenicono = '<img src="' . __AUZALAN_DIR__ . '/css/icono.png" width="78px"/>';

		if ($array[$i]['imagen'] != Null){
				$imagen='<figure><img src="' . $array[$i]['imagen'] . '" /></figure>';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['titulo'];
		
		/* enlaces */
		$enlace_izquierda='';
		
		$enlace_derecha='';
		if ($array[$i]['url']!=Null){ 
				$enlace_derecha='<a href="' . $array[$i]['url'] .'" target="_blank">' . $lng_inscripcion . '</a>';				
		}
		
		/* TEXTOS */
		$contenido='';
		if ($array[$i]['descripcion']!=Null){ 
				 $contenido=trim($array[$i]['descripcion']);
		}
		if ($array[$i]['objetivos']!=Null){ $contenido .='<br><b>' . $lng_Objetivos . '</b>: ' .  $array[$i]['objetivos'] .'<br>';}
		if ($array[$i]['programa']!=Null){  $contenido .='<br><b>' . $lng_Programa . '</b>: ' .  $array[$i]['programa'] .'<br>';}
		
		if ($array[$i]['requisitos']!=Null){  $contenido .='<br><b>' . $lng_Requisitos . '</b>: ' .  $array[$i]['requisitos'] .'<br>';}
		if ($array[$i]['tipoActividad']!=Null){  $contenido .='<br><b>' . $lng_TipoActividad . '</b>: ' .  $array[$i]['tipoActividad'] .'<br>';}
		if ($array[$i]['areaTematica']!=Null){  $contenido .='<br><b>' . $lng_AreaTematica . '</b>: ' .  $array[$i]['areaTematica'] .'<br>';}
		if ($array[$i]['lugarCelebracion']!=Null){  $contenido .='<br><b>' . $lng_LugarCelebracion . '</b>: ' .  $array[$i]['lugarCelebracion'] .'<br>';}
		if ($array[$i]['horasLectivas']!=Null){ $contenido .='<br><b>' . $lng_HorasLectivas . '</b>: ' .  $array[$i]['horasLectivas'] .'<br>';}


		if ($array[$i]['fechaLimite']!=Null){ $contenido .='<br><b>' . $lng_FlimiteInscripcion . '</b>: ' .  $array[$i]['fechaLimite'] .'<br>';}
		if ($array[$i]['fechaInicio']!=Null){ $contenido .='<br><b>' . $lng_FlimiteInicio . '</b>: ' .  $array[$i]['fechaInicio'] .'<br>';}
		if ($array[$i]['fechaFin']!=Null){ $contenido .='<br><b>' . $lng_FlimiteFin . '</b>: ' .  $array[$i]['fechaFin'] .'<br>';}
		
        if ($array[$i]['horario']!=Null){ $contenido .='<br><b>' . $lng_Calendario . '</b>: ' .  $array[$i]['horario'] .'<br>';}
		
		if(count($array[$i]['precios']) > 0){
			$contenido .='<div class="FormAuz_columnas_1" >';
			$contenido .='</div>';
				if ($array[$i]['precios']['descripcion1']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion1'] . '</b>: ' . $array[$i]['precios']['precio1'] .' €<br>';}
				if ($array[$i]['precios']['descripcion2']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion2'] . '</b>: ' . $array[$i]['precios']['precio2'] .' €<br>';}
				if ($array[$i]['precios']['descripcion3']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion3'] . '</b>: ' . $array[$i]['precios']['precio3'] .' €<br>';}
				if ($array[$i]['precios']['descripcion4']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion4'] . '</b>: ' . $array[$i]['precios']['precio4'] .' €<br>';}
				if ($array[$i]['precios']['descripcion5']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion5'] . '</b>: ' . $array[$i]['precios']['precio5'] .' €<br>';}
				if ($array[$i]['precios']['descripcion6']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion6'] . '</b>: ' . $array[$i]['precios']['precio6'] .' €<br>';}
		}
		if ($array[$i]['subvencionado']!=Null){ $contenido .='<br><b>' . $lng_CursoSubvencionado . '</b>: ' .  $array[$i]['subvencionado'] .'<br>';}
		 
		 
		 
		 
		/* ADJUNTO */
		$adjunto='';
		if ($array[$i]['adjunto'] != Null){
				$adjunto='<a href="' . $array[$i]['adjunto'] . '" target="_blank"/>' . $lng_DescargarAdjunto . '</a>';
		}
		$textoampliado = $contenido . '<br>' . $adjunto . '<br>';
		
		 
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			' . $imagen . '	
			<div class="contenido">
				<div class="titulo"> 
					<div class="tituloicono">' . $imagenicono . '</div>
					<div class="titulotexto"><h3>' . $titulo . '</h3></div>
				</div>
				<section class="textoampliado">' . $textoampliado . '</section>				
				<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			</div>
		</div>
		</li>
		';
	} 
	}else{
		$error= '<!-- sin conexion --> ';
	 
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo4.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
