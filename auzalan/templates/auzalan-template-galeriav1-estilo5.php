<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria estilo="estilox" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*
*   MISMA HOJA ESTILOS QUE ESTILO4
*/


/*                                     ESTILO 3        */
function auzalan_template_galeria_v1_estilo5( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' estilo5 --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
    
	
		$id='auz_id_' . $array[$i]['id'];
		
		$id_texto_reducido='auz_id_txt_red_' . $array[$i]['id'];
		$id_enlace='auz_id_enlace_' . $array[$i]['id'] ;
		
		$imagen='';
		$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
		$imagenicono = '<img src="' . __AUZALAN_WEB_DIRECTORIO__ .$categoriaIcono.'.png" width="78px"/>';
			
		if ($array[$i]['Imagen'] != Null){
				$imagen='<figure><img src="' . $array[$i]['Imagen'] . '" /></figure>';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['Titulo'];
		
		/* enlaces */
		$enlace_izquierda='';
		
		$enlace_derecha='';
		if ($array[$i]['Url']!=Null){ 
				$enlace_derecha='<a href="' . $array[$i]['Url'] .'" target="_blank">inscripción</a>';				
		}
		
		/* TEXTOS */
		/*elimino ademas strip_tags etiquetas html, ver como puedo permitir algunas en http://php.net/manual/es/function.strip-tags.php*/
		 
		 
		$contenido='';
		if ($array[$i]['Contenido']!=Null){ 
				 $contenido=trim($array[$i]['Contenido']);
		}
		
			if ($contenido != ''){
			  $textoreducido= strip_tags($contenido);    /* quito ETIQUETAS HTML AL TEXTO REDUCIDO*/
				$textoampliado= $contenido;
				
				if (strlen($contenido)>__MAX_LENGTH__){
				
				$textoreducido= strip_tags($contenido);
				$textoreducido= substr($textoreducido,0,__MAX_LENGTH__) . '...';
				/*$enlace_izquierda='<a href="#" id="'. $id_enlace .'" onclick="auz_template_galeriav1_mostrar_ocultar_texto(\''. $id .'\',\''. $id_texto_reducido .'\',\''. $id_enlace .'\')">ver más</a>';
				*/
				$enlace_izquierda='<a href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">ver más</a>';
					
				}				
			}
		 
 
		$adjunto='';
		if ($array[$i]['Adjunto'] != Null){
				$adjunto='<a href="' . $array[$i]['Adjunto'] . '" target="_blank"/>descargar adjunto</a>';
		}
		$textoreducido = $textoreducido . '<br>' . $adjunto . '<br>';
	 
		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			' . $imagen . '	
			<div class="contenido">
				<div class="titulo"> 
					<div class="tituloicono">' . $imagenicono . '</div>
					<div class="titulotexto"><h3>' . $titulo . '</h3></div>
				</div>
				<section class="textoreducido">' . $textoreducido . '</section>		
				<section class="auz_enlace_izquierda">' . $enlace_izquierda . '</section>				
				<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			</div>
		</div>
		</li>
		';
	}else{
		$error= '<!-- sin conexion --> ';
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo4.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
