<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 				[plg_auzalan_galeria bdd="formacion" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO 2         */
function auzalan_formacion_template_galeria_v1_estilogippre( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	// ----- idioma ----------------- ----------------- ----------------- -----------------
	$segundoidioma="";
	$lng_verMas="Ver más";
	$lng_verMenos="Ver menos";
	$lng_verPublicacion="Ver publicación";
	$lng_verVideo="Ver vídeo";
	$lng_inscripcion="Inscripcion";
		 
		if ((!empty($atts['idioma']))&&($atts['idioma']=='idioma2')) {	
				 $segundoidioma="&idioma=" . $atts['idioma'];
				 $lng_verMas="Gehiago ikusi";
				 $lng_verMenos="Gutxiago ikusi";
				 $lng_verPublicacion="Ikusi argitalpena";
				 $lng_verVideo="Ikusi bideoa";
				 $lng_inscripcion="Inskripzioa";
		}
	// -----fin idioma ----------------- ----------------- ----------------- ----------------- -----------------
	
	$galeria = "";
	
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['idPb'])) {
		/*DECLARACIÓN DE VARIABLES*/
		$id='auz_form_id_' . $array[$i]['idPb'] ;   /*  USADO PARA SALTAR A NODO INDIVIDUAL O ABRIR TEXTO A VER MÁS*/
		$id_texto_reducido='auz_form_id_txt_red_' . $array[$i]['idPb'] ;
		$id_enlace='auz_form_id_enlace_' . $array[$i]['idPb'] ;
		/*DECLARACIÓN DE VARIABLES*/
		/* bucle +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
		$imagen = '<img src="' . __AUZALAN_DIR__ . '/css/icono.png" width="78px"/>';
		if ($array[$i]['imagen'] != Null){
				$imagen='<img src="' . $array[$i]['imagen'] . '" />';
		}
		/* TITULOS*/
		$titulo=$array[$i]['titulo'];
		$titulo='<a href="'.__FORMACION_AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['idPb'].   $segundoidioma . '">'. $titulo .'</a>';
		
		/* enlaces */
		
		/* ADJUNTO */
		$enlace_izquierda='';
		if ($array[$i]['adjunto'] != Null){
				$enlace_izquierda='<a href="' . $array[$i]['adjunto'] . '" target="_blank"  class="auz_enlace_gipverde"/>' . $lng_verMas . '</a>';
		}
		
		$enlace_centro='';
		if ($array[$i]['EnlaceVideo'] != Null){
		$enlace_centro='<section class="auz_enlace_centro"><a href="' . $array[$i]['EnlaceVideo'] .'" target="_blank" class="auz_enlace_gipverde">' . $lng_verVideo . '</a></section>';
		}
		
		$enlace_derecha='<a href="' . $array[$i]['url'] .'" target="_blank" class="auz_enlace_gipverde">' . $lng_inscripcion . '</a>';
		
		/* TEXTOS */
		$textoreducido='';
		$textoampliado='';
		$contenido='';
		 
		
		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			<figure>' . $imagen . '		</figure>
			<h3>' . $titulo . '</h3>
			
			'. $enlace_centro .'
			<section class="auz_enlace_izquierda">' . $enlace_izquierda . '</section>
			<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			</div>
		</li>
		';
		
		/* bucle +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
	}else{
		$error= '<!-- sin conexion --> ';
	}
	}
		
	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilogippre.css" type="text/css">';
	/*  si gestiono desde aquí el número de columnas no lo hace responsive $estilo  .=  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>';  */
	
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
    //------------- scripts -----------------------------------------------------------------------------


	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
