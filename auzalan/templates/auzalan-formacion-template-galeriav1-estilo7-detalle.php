<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 				[plg_auzalan_galeria bdd="formacion" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*
*   ESTILO DE ALBACETE
*/


/*        formacion                             ESTILO 7        */
function auzalan_formacion_template_galeria_v1_estilo7_detalle( $array, $atts,$idioma ) {
 
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	
	
	// ----- idioma ----------------- ----------------- ----------------- -----------------
	
	$lng_Objetivos="Objetivos";
	$lng_Programa="Programa";
	$lng_Requisitos="Requisitos";
	$lng_TipoActividad="Tipo de actividad";
	$lng_AreaTematica="Area temática";
	$lng_LugarCelebracion="Lugar de celebración";
	$lng_HorasLectivas="Horas lectivas";
	$lng_FlimiteInscripcion="Fecha límite de Inscripción";
	$lng_FlimiteInicio="Fecha inicio del curso";
	$lng_FlimiteFin="Fecha fin del curso";
	$lng_Calendario="Calendario";
	$lng_CursoSubvencionado ="Curso Subvencionado";
	$lng_DescargarAdjunto ="descargar adjunto";
	$lng_verMas="Ver más";
	$lng_verMenos="Ver menos";
	$lng_verPublicacion="Ver publicación";
	$lng_verVideo="Ver vídeo";
	$lng_inscripcion="inscripcion";
	
		if ($idioma == 'idioma2')  {	
				 $lng_Objetivos="Helburuak";
				 $lng_Programa="Programa";
				 $lng_Requisitos="Baldintzak";
				 $lng_TipoActividad="Jarduera mota";
				 $lng_AreaTematica="Gaikako arloa";
				 $lng_LugarCelebracion="Ospakizun lekua";
				 $lng_HorasLectivas="Irakaskuntza orduak";
				 $lng_FlimiteInscripcion="Izena emateko epea";
				 $lng_FlimiteInicio="Ikastaroaren hasiera data";
				 $lng_FlimiteFin="Ikastaroaren amaiera data";
				 $lng_CursoSubvencionado ="Diruz lagundutako ikastaroa";
				 $lng_Calendario="Egutegia";
				 $lng_DescargarAdjunto ="eranskina deskargatu";
				 $lng_verMas="Gehiago ikusi";
				 $lng_verMenos="Gutxiago ikusi";
				 $lng_verPublicacion="Ikusi argitalpena";
				 $lng_verVideo="Ikusi bideoa";
				 $lng_inscripcion="inskripzioa";
		}
	// ----- fin idioma ----------------- ----------------- ----------------- ----------------- ----------------- -----------------
	
	
	
	
	for($i=0;$i<count($array);$i++){
     $mostrar=1;
	 if (!empty($array[$i]['idPb'])) {
	/*
		ERROR SI INCLUYO ATTS...
	*/
	 if ( $mostrar==1){
		$imagen='';
		$imagenicono = '<img src="' . __AUZALAN_DIR__ . '/css/icono.png" width="78px"/>';

		if ($array[$i]['imagen'] != Null){
				$imagen=$array[$i]['imagen'];
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['titulo'];
		
		
		
		/* TEXTOS */
		$contenido='<b>' . mb_strtoupper($titulo) . '</b><br><br>';
		if ($array[$i]['descripcion']!=Null){ 
				 $contenido .=trim($array[$i]['descripcion']);
		}
		if ($array[$i]['objetivos']!=Null){ $contenido .='<br><b>' . $lng_Objetivos . '</b>: ' .  $array[$i]['objetivos'] .'<br>';}
		if ($array[$i]['programa']!=Null){  $contenido .='<br><b>' . $lng_Programa . '</b>: ' .  $array[$i]['programa'] .'<br>';}
		
		if ($array[$i]['requisitos']!=Null){  $contenido .='<br><b>' . $lng_Requisitos . '</b>: ' .  $array[$i]['requisitos'] .'<br>';}
		if ($array[$i]['tipoActividad']!=Null){  $contenido .='<br><b>' . $lng_TipoActividad . '</b>: ' .  $array[$i]['tipoActividad'] .'<br>';}
		if ($array[$i]['areaTematica']!=Null){  $contenido .='<br><b>' . $lng_AreaTematica . '</b>: ' .  $array[$i]['areaTematica'] .'<br>';}
		if ($array[$i]['lugarCelebracion']!=Null){  $contenido .='<br><b>' . $lng_LugarCelebracion . '</b>: ' .  $array[$i]['lugarCelebracion'] .'<br>';}
		if ($array[$i]['horasLectivas']!=Null){ $contenido .='<br><b>' . $lng_HorasLectivas . '</b>: ' .  $array[$i]['horasLectivas'] .'<br>';}


		if ($array[$i]['fechaLimite']!=Null){ $contenido .='<br><b>' . $lng_FlimiteInscripcion . '</b>: ' .  $array[$i]['fechaLimite'] .'<br>';}
		if ($array[$i]['fechaInicio']!=Null){ $contenido .='<br><b>' . $lng_FlimiteInicio . '</b>: ' .  $array[$i]['fechaInicio'] .'<br>';}
		if ($array[$i]['fechaFin']!=Null){ $contenido .='<br><b>' . $lng_FlimiteFin . '</b>: ' .  $array[$i]['fechaFin'] .'<br>';}
		
        if ($array[$i]['horario']!=Null){ $contenido .='<br><b>' . $lng_Calendario . '</b>: ' .  $array[$i]['horario'] .'<br>';}
		
		if(count($array[$i]['precios']) > 0){
			$contenido .='<div class="FormAuz_columnas_1" >';
			$contenido .='</div>';
				if ($array[$i]['precios']['descripcion1']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion1'] . '</b>: ' . $array[$i]['precios']['precio1'] .' €<br>';}
				if ($array[$i]['precios']['descripcion2']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion2'] . '</b>: ' . $array[$i]['precios']['precio2'] .' €<br>';}
				if ($array[$i]['precios']['descripcion3']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion3'] . '</b>: ' . $array[$i]['precios']['precio3'] .' €<br>';}
				if ($array[$i]['precios']['descripcion4']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion4'] . '</b>: ' . $array[$i]['precios']['precio4'] .' €<br>';}
				if ($array[$i]['precios']['descripcion5']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion5'] . '</b>: ' . $array[$i]['precios']['precio5'] .' €<br>';}
				if ($array[$i]['precios']['descripcion6']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion6'] . '</b>: ' . $array[$i]['precios']['precio6'] .' €<br>';}
		}
		if ($array[$i]['subvencionado']!=Null){ $contenido .='<br><b>' . $lng_CursoSubvencionado . '</b>: ' .  $array[$i]['subvencionado'] .'<br>';}
		 
		 
		 
		 
		/* ADJUNTO 
		$adjunto='';
		if ($array[$i]['adjunto'] != Null){
				$adjunto='<a href="' . $array[$i]['adjunto'] . '" target="_blank"/>' . $lng_DescargarAdjunto . '</a>';
		}
		$textoampliado = $contenido . '<br>' . $adjunto . '<br>';
		*/
		
		/* enlaces 
		$enlace_izquierda='';
		
		$enlace_derecha='';
		if ($array[$i]['url']!=Null){ 
				$enlace_derecha='<a href="' . $array[$i]['url'] .'" target="_blank">' . $lng_inscripcion . '</a>';				
		}*/
		$enlace='';
				if ($array[$i]['url']!=Null){ 
					$enlace='
					<div style="margin-bottom:15px" class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="' . $array[$i]['url'] .
					'" data-icon="$">Inscripción</a></div>';				
			}
		$adjunto='';
			if ($array[$i]['adjunto']!=Null){ 
				$adjunto='
				 <div style="margin-bottom:15px" class="et_pb_button_wrapper"><a   target="_blank"  class="et_pb_button et_pb_promo_button" href="' . $array[$i]['adjunto'] .
				'" data-icon="$">Descargar Adjunto</a></div>';				
		}
		
		
		$contenido=$contenido . '<br><br>' . $adjunto  . $enlace;	

		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<link rel="stylesheet" href="http://new.aparejadoresalbacete.es/wp-content/plugins/auzalan/css/auzalan-formacion-template-galeriav1-estilo7-detalle.css" type="text/css">
<div class="et_pb_row et_pb_row_1 et_pb_gutters3">	

	<div class="et_pb_module et_pb_image et_pb_image_0">				
				<span class="et_pb_image_wrap has-box-shadow-overlay"><div class="box-shadow-overlay"></div><img  src="' . $imagen . '	
			" alt="" title=""  class="auzalan_mi_sombra" /></span>
	</div>
	<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_left et_pb_bg_layout_light et_pb_no_bg">' . $contenido . '</div>
</div>';
	} 
	}else{
		$error= '<!-- sin conexion --> ';
	 
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo4.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------



	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
