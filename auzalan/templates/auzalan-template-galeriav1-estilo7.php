<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria estilo="estilox" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO 7 Y 7 PORTADA   SOLO PARA LA HOJA DE ESTILOS DIVI DE ALBACETE   */
function auzalan_template_galeria_v1_estilo7( $array, $atts) {
	$error= '';
	$version ='<!-- -------------------------------- AUZALAN VERSION: '. __AUZ_VERSION__ .' auzalan_template_galeria_v1_estilo7 -->';
	$content='';
	$content0cab='<div class="et_pb_row et_pb_row_1 et_pb_gutters3">';
	$content0pie='</div>';
	$content1='	
<div class="et_pb_row et_pb_row_1 et_pb_gutters3 et_pb_row_1-4_3-4">
	<div class="et_pb_column et_pb_column_1_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
		<div class="et_pb_module et_pb_image et_pb_image_0">
			<span class="et_pb_image_wrap has-box-shadow-overlay">
				<div class="box-shadow-overlay"></div>';
	$content2='		
			</span>
		</div>		
	</div>		
	<div class="et_pb_column et_pb_column_3_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">
		<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_left et_pb_bg_layout_light et_pb_no_bg">
				<div class="et_pb_promo_description">
					<h2 class="et_pb_module_header">';
	$content3='	</h2>
					<div>';
	$content4='</div>
				</div>
				<div class="et_pb_button_wrapper">
					<a class="et_pb_button et_pb_promo_button" href="';
	$content5='" data-icon="$">Leer más</a>
				</div>
		</div>
	</div>	
</div>	';
	$pie='<!-- --------------------------------< FIN AUZALAN - -->';
	
	
	
	for($i=0;$i<count($array);$i++){
		if (!empty($array[$i]['id'])) {			
			$imagen='';
			$title=$array[$i]['Titulo'];
			$textoreducido='';
			
			if ($array[$i]['Contenido']!=Null){ 
				 $textoreducido=trim($array[$i]['Contenido']);
				 $textoreducido= strip_tags($textoreducido);    /* quito ETIQUETAS HTML AL TEXTO REDUCIDO*/
				 $textoreducido= substr($textoreducido,0,__MAX_LENGTH__) . '...';
				 
			}
			
			if ($array[$i]['Imagen'] != Null){
				$imagen='<img src="' . $array[$i]['Imagen'] . '" alt="" title=""  class="wp-image-178 auzalan_mi_sombra"  >';
			}
			$url=__AUZALAN_POST_VIEWER_PAGE__ . '?postid='. $array[$i]['id'];
			
			$content =$content . $content1 . $imagen . $content2 . $title . $content3 . $textoreducido . $content4 . $url . $content5;
		}
	}
	
	$estilo = ' <link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo7.css" type="text/css"> ';
	
	if ($error == '' ){
		$devuelvo =$version . $estilo . $content0cab . $content . $content0pie . $pie ;
	}else	{
		$devuelvo =$error;
	}
	
	
	
	
return ($devuelvo);
}


function auzalan_template_galeria_v1_estilo7p( $array, $atts) {
	$error= '';
	$version ='<!-- -------------------------------- AUZALAN VERSION: '. __AUZ_VERSION__ .' estilo7p portada -->';
	$pie='<!-- --------------------------------< FIN AUZALAN - -->';
	
	$contenido='';
	
	for($i=0;$i<count($array);$i++){
		if (!empty($array[$i]['id'])) {		


			

		
			$imagen='https://aparejadoresalbacete.es/wp-content/uploads/2022/04/NOVEDADES-copia.png';
			$title=$array[$i]['Titulo'];
			 
			 
			
			if ($array[$i]['Imagen'] != Null){
				$imagen =$array[$i]['Imagen'];
			}
			$url=__AUZALAN_POST_VIEWER_PAGE__ . '?postid='. $array[$i]['id'];
			
			
			
			$contenido=$contenido."
				<li><div class='et_pb_blurb_content'>
								<div class='et_pb_main_blurb_image'><a href='" . $url . "'><span class='et_pb_image_wrap et_pb_only_image_mode_wrap has-box-shadow-overlay'><img src='" . $imagen . "' alt='' class='et-waypoint et_pb_animation_top et_pb_animation_top_tablet et_pb_animation_top_phone wp-image-1921 et-animated auzalan_mi_sombra' width='300' height='200'></span></a></div>
								<div class='et_pb_blurb_container'>
									<h5 class='et_pb_module_header'><a href='" . $url . "'>" . $title . "</a></h5>									
								</div>
							</div></li>
				";
			 
		}
	}
	
	
	
	
	
	
	$estilo = ' <link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo7p.css" type="text/css"> ';
	
	if ($error == '' ){
		$devuelvo =$version . $estilo . "<ul class='auz_gallery_v1'>" .  $contenido . "</ul>" .  $pie ;
	}else	{
		$devuelvo =$error;
	}
	
	
	
	
return ($devuelvo);
}

function auzalan_template_galeria_v1_estilo7_3variascolumnas( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 	   
		. __AUZ_VERSION__ .' 7_3variascolumnas --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$galeria = "";
	/* IMAGEN */
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
    
	
		$id='auz_id_' . $array[$i]['id'];
		
		$id_texto_reducido='auz_id_txt_red_' . $array[$i]['id'];
		$id_enlace='auz_id_enlace_' . $array[$i]['id'] ;
		
		$imagen='';
		$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
		$imagen = '<img src="' . __AUZALAN_DIR__ . 'imagen.png"  width="400px" height="300px" class="auzalan_caja_sombra"/>';
		$imagenicono ='';
			
		if ($array[$i]['Imagen'] != Null){
				$imagen='<img src="' . $array[$i]['Imagen'] . '" width="400px" height="300px" class="auzalan_caja_sombra"/>';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['Titulo'];
		
		$textoreducido='&nbsp;';
		$textoampliado='';
		$contenido='';
		
		/* enlaces */
		$enlace_izquierda='';
		$enlace_derecha='';
		//$leermas='<a href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">Leer más</a>';
		//$leermas='<div class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" data-icon="$">Leer más</a></div>';
		//$leermas='<a class="et_pb_button et_pb_button_0 et_animated et_hover_enabled et_pb_bg_layout_light" href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" data-icon="&#x45;">Leer más</a>';
		$leermas='<div class="auzleermas_caja">
					<a class="auzleermas" 
					href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" 
					data-icon="&#x45;">Leer más</a>
					</div>';
		$titulo=$array[$i]['Titulo']; 


		/*if ($array[$i]['Url']!=Null){ 
				
				$enlace_derecha='<img src="' . __AUZALAN_WEB_DIRECTORIO__ .'web.png" />&nbsp;<a href="' . $array[$i]['Url'] .'" target="_blank">ir a publicación</a>';
				$titulo='<a href="' . $array[$i]['Url'] .'" target="_blank">'. $titulo . '</a>';
				$imagen='<a href="' . $array[$i]['Url'] .'" target="_blank">'. $imagen . '</a>';
				$link = $array[$i]['Url'];
		}
		if ($array[$i]['Adjunto']!=Null){ 
				$enlace_derecha='<img src="' . __AUZALAN_WEB_DIRECTORIO__ .'adjunto.png" /><br><br><a href="' . $array[$i]['Adjunto'] . '" target="_blank"/>descargar adjunto</a>';
				$titulo='<a href="' . $array[$i]['Adjunto'] .'" target="_blank">'. $titulo . '</a>';
				$imagen='<a href="' . $array[$i]['Adjunto'] .'" target="_blank">'. $imagen . '</a>';
				$link = $array[$i]['Adjunto'];
		}		 */
	 
		// al final no ponemos ningun enlace, en la definitiva que se abra todo
		
	 
		
		//$textoampliado .= '<br>' . $adjunto . '<br>';   <figure class="auzalan_caja_sombra">' . $imagen . '</a></figure>
		 
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">' . $imagen . '			
			<div class="et_pb_text_inner auzalan_centrar_texto" >' . $titulo . ' <br><br> ' . $leermas . '</div>
		</div>
		</li>
		';
	}else{
		$error= '<!-- sin conexion --> ';
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo7-variascolumnas.css" type="text/css">';
 
	
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}


function auzalan_template_galeria_v1_estilo7_3variascolumnas_sinImagen( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 	   
		. __AUZ_VERSION__ .' 7_3variascolumnas --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$galeria = "";
	/* IMAGEN */
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
    
	
		$id='auz_id_' . $array[$i]['id'];
		
		$id_texto_reducido='auz_id_txt_red_' . $array[$i]['id'];
		$id_enlace='auz_id_enlace_' . $array[$i]['id'] ;
		
		$imagen='';
		$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
		//$imagen = '<img src="' . __AUZALAN_DIR__ . 'imagen.png"  width="400px" height="300px" class="auzalan_caja_sombra"/>';
		$imagenicono ='';
			
		//if ($array[$i]['Imagen'] != Null){
		//		$imagen='<img src="' . $array[$i]['Imagen'] . '" width="400px" height="300px" class="auzalan_caja_sombra"/>';
		//}
		
		/* TITULOS*/
		$titulo=$array[$i]['Titulo'];
		
		$textoreducido='&nbsp;';
		$textoampliado='';
		$contenido='';
		
		/* enlaces */
		$enlace_izquierda='';
		$enlace_derecha='';
		//$leermas='<a href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">Leer más</a>';
		//$leermas='<div class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" data-icon="$">Leer más</a></div>';
		//$leermas='<a class="et_pb_button et_pb_button_0 et_animated et_hover_enabled et_pb_bg_layout_light" href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" data-icon="&#x45;">Leer más</a>';
		$leermas='<div class="auzleermas_caja">
					<a class="auzleermas" 
					href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" 
					data-icon="&#x45;">Leer más</a>
					</div>';
		$titulo=$array[$i]['Titulo']; 


		/*if ($array[$i]['Url']!=Null){ 
				
				$enlace_derecha='<img src="' . __AUZALAN_WEB_DIRECTORIO__ .'web.png" />&nbsp;<a href="' . $array[$i]['Url'] .'" target="_blank">ir a publicación</a>';
				$titulo='<a href="' . $array[$i]['Url'] .'" target="_blank">'. $titulo . '</a>';
				$imagen='<a href="' . $array[$i]['Url'] .'" target="_blank">'. $imagen . '</a>';
				$link = $array[$i]['Url'];
		}
		if ($array[$i]['Adjunto']!=Null){ 
				$enlace_derecha='<img src="' . __AUZALAN_WEB_DIRECTORIO__ .'adjunto.png" /><br><br><a href="' . $array[$i]['Adjunto'] . '" target="_blank"/>descargar adjunto</a>';
				$titulo='<a href="' . $array[$i]['Adjunto'] .'" target="_blank">'. $titulo . '</a>';
				$imagen='<a href="' . $array[$i]['Adjunto'] .'" target="_blank">'. $imagen . '</a>';
				$link = $array[$i]['Adjunto'];
		}		 */
	 
		// al final no ponemos ningun enlace, en la definitiva que se abra todo
		
	 
		
		//$textoampliado .= '<br>' . $adjunto . '<br>';   <figure class="auzalan_caja_sombra">' . $imagen . '</a></figure>
		 
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">' . $imagen . '			
			<div class="et_pb_text_inner auzalan_centrar_texto" >' . $titulo . ' <br><br> ' . $leermas . '</div>
		</div>
		</li>
		';
	}else{
		$error= '<!-- sin conexion --> ';
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo7-variascolumnas.css" type="text/css">';
 
	
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}

