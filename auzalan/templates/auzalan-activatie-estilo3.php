<?php

/**
* 2020-septiembre (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria bdd="activatie" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO 3        */
function auzalan_activatie_estilo3( $arrayCurso, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' estilo3 --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$num_nodosMaximo=__AUZALAN_MAX_NODOS__;
	if(is_numeric($atts["num_nodos"])) {
		$num_nodosMaximo=(int)$atts["num_nodos"];
	}
	$galeria = "";
	$array=$arrayCurso['curso'];
	$j=0;
	for($i=0;$i<count($array);$i++){
	
	if (!empty($array[$i]['id'])) {
		$j++;
	if ($j<=$num_nodosMaximo){
    
	
		$id='auz_form_id_' . $array[$i]['id'] ;   /*  USADO PARA SALTAR A NODO INDIVIDUAL O ABRIR TEXTO A VER MÁS*/
		$id_texto_reducido='auz_form_id_txt_red_' . $array[$i]['id'] ;
		$id_enlace='auz_form_id_enlace_' . $array[$i]['id'] ;
		/*DECLARACIÓN DE VARIABLES*/
		/* bucle +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
		$imagenicono = '<img src="' . __AUZALAN_DIR__ . '/css/icono.png" width="78px"/>';
		if ($array[$i]['imagen'] != Null){
				$imagen='<img src="' . $array[$i]['imagen'] . '" />';
		}
		/* TITULOS*/
		$titulo=$array[$i]['nombre'];
		$titulo='<a href="'.__AUZALAN_ACTIVATIE_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">'. $titulo . '</a>';
		
		/* enlaces */
		$enlace_izquierda='<a href="'.__AUZALAN_ACTIVATIE_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">ver más</a>';
		
		$enlace_derecha='<a href="' . $array[$i]['link'] .'" target="_blank">inscripción</a>';
		
		/* TEXTOS */
		// no hay textos
		$contenido='&nbsp;';
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			<figure>' . $imagen . '		</figure>
			<h3>' . $titulo . '</h3>
			<figure class="auz_enlace_centro">' . $imagenicono . '</figure>
			<section class="auz_textoreducido">' . $contenido . '</section>
			<section class="auz_enlace_izquierda">' . $enlace_izquierda . '</section>
			<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			
			</div>
		</li>
		';
		
		
	}
	}else{
		$error= '<!--008--- auzalan sin conexion --> ';
	}  // FIN EMPTY
	
	} // FIN FOR

//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo3.css" type="text/css">';
	/*  si gestiono desde aquí el número de columnas no lo hace responsive $estilo  .=  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>';  */
	
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
