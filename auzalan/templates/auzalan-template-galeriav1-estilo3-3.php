<?php

/**
* (ULTIMA MODIFICACIÓN 2021-10-07)
* UN SOLO  ENLACE O PUBLICACIÓN O ADJUNTO. 
* 			[plg_auzalan_galeria estilo="estilo3_3" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO 3        */
function auzalan_template_galeria_v1_estilo3_3( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' estilo3-3 --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$galeria = "";
	/* IMAGEN */
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
    
	
		$id='auz_id_' . $array[$i]['id'];
		
		$id_texto_reducido='auz_id_txt_red_' . $array[$i]['id'];
		$id_enlace='auz_id_enlace_' . $array[$i]['id'] ;
		
		$imagen='';
		$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
		$imagen = '<img src="' . __AUZALAN_WEB_DIRECTORIO__ .$categoriaIcono.'.png" width="40px"/>';
		$imagenicono =$imagen;
			
		if ($array[$i]['Imagen'] != Null){
				$imagen='<img src="' . $array[$i]['Imagen'] . '" />';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['Titulo'];
		$textoreducido='&nbsp;';
		$textoampliado='';
		$contenido='';
		
		/* enlaces */
		$enlace_izquierda='';
		$enlace_derecha='';


		if ($array[$i]['Url']!=Null){ 
				$enlace_derecha='<img src="' . __AUZALAN_WEB_DIRECTORIO__ .'web.png" />&nbsp;<a href="' . $array[$i]['Url'] .'" target="_blank">ir a publicación</a>';
				$titulo='<a href="' . $array[$i]['Url'] .'" target="_blank">'. $titulo . '</a>';
				$imagen='<a href="' . $array[$i]['Url'] .'" target="_blank">'. $imagen . '</a>';
		}
		if ($array[$i]['Adjunto']!=Null){ 
				$enlace_derecha='<img src="' . __AUZALAN_WEB_DIRECTORIO__ .'adjunto.png" />&nbsp;<a href="' . $array[$i]['Adjunto'] . '" target="_blank"/>descargar adjunto</a>';
				$titulo='<a href="' . $array[$i]['Adjunto'] .'" target="_blank">'. $titulo . '</a>';
				$imagen='<a href="' . $array[$i]['Adjunto'] .'" target="_blank">'. $imagen . '</a>';
		}		 
	 
		 
		
		//$textoampliado .= '<br>' . $adjunto . '<br>';
		$enlace_izquierda='';
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			<figure>' . $imagen . '</a></figure>
			<h3>' . $titulo . '</h3>
			<section class="auz_textoreducido" id="'. $id_texto_reducido. '"><p>' . $textoreducido . '</p></section>
			<section class="auz_textoampliado" id="'. $id .'"><p>' . $textoampliado . '</p></section>
			<section class="auz_enlace_izquierda">' . $imagenicono . '</section>
			<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			
			</div>
		</li>
		';
	}else{
		$error= '<!-- sin conexion --> ';
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo3.css" type="text/css">';
 
	
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
