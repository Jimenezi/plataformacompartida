<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 				[plg_auzalan_galeria bdd="formacion" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*
*   HOJA NEW ALBACETE
*/


/*        formacion                             ESTILO 7   y CABECERA MAS ABAJO     */
function auzalan_formacion_template_galeria_v1_estilo7( $array, $atts) {

	$error= '';
	$contenedorIni='<!-- --------------------------------< AUZALAN VERSION: ' . __AUZ_VERSION__ .' PLANTILLA: auzalan-formacion-template-galeriav1-estilo7  FUNCION: auzalan_formacion_template_galeria_v1_estilo7 > -->';
	$contenedorFin='<!-- --------------------------------< FIN AUZALAN >- -->';
	
	$num_columnas = 3;
	$num_columnas_contador = 1;
	/*  SE OMITE COMPROBACIÓN, LA PÁGINA ESTÁ PREPARADA PARA 3 O 1 NODOS SEGÚN TAMAÑO
	if(is_numeric($atts["num_columnas"])) {
			$num_columnas = $atts["num_columnas"];
	}
	*/
	$contenido = "";
	/* plantillas */
	
	$contenidoFila_head='<div class="et_pb_row et_pb_row_1 et_pb_gutters3">';
	$contenidoFila_foot='</div>';
	
	$contenidoNodo_0='<div class="et_pb_column et_pb_column_1_3 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough">
		<div class="et_pb_module_auzalan et_pb_image et_pb_image_0">';
	$contenidoNodo_1='</div>
		<div class="et_pb_with_border et_pb_module_auzalan et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_center et_pb_bg_layout_light et_pb_no_bg">
			<div class="et_pb_promo_description">';
	$contenidoNodo_2='</div>
			<div class="et_pb_button_wrapper et_pb_text_align_center">
				<a class="et_pb_button et_pb_promo_button" href="';
	$contenidoNodo_3='" data-icon="&#x24;">Leer más</a>
			</div>
		</div>
	</div>';
	
	for($i=0;$i<count($array);$i++){
     $mostrar=1;
	 if (!empty($array[$i]['idPb'])) {
	 
	 if(is_numeric($atts["colegio_excluye"])) {
		  if($atts["colegio_excluye"]==$array[$i]['CidO']) {
			$mostrar=0;			
		  }	
	 }
	 
	 if(is_numeric($atts["colegio"])) {
		  if($atts["colegio"]==$array[$i]['CidO']) {
			$mostrar=1;
			}else{
			$mostrar=0;
		  }	
	 }
 
     if ( $mostrar==1){
		$titulo=mb_strtoupper($array[$i]['titulo']);
		$imagen='';		
		$url='';
		
		if (strlen($titulo)>__MAX_LENGTH_caracteresTitulo_estilo7__){
			$titulo= substr($titulo,0,__MAX_LENGTH_caracteresTitulo_estilo7__) . '...';
		}
		 
		
		$titulo='<div>' . $titulo . '</div>';
		$url=__FORMACION_AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['idPb'];
		if ($array[$i]['imagen'] != Null){
				$imagen='<span class="et_pb_image_wrap has-box-shadow-overlay"><div class="box-shadow-overlay"></div><a href="' . $url . '"><img  src="' . $array[$i]['imagen'] . '" alt="" title=""  class="auzalan_mi_sombra" /></a></span>';
		}
		
		 
		/* UNIR EL ESTILO ENTERO */
		
		if(	$num_columnas_contador == 1 ){
			// si es el primero de la FILA INSERTO HEAD  e incremento contador
			$contenido .= $contenidoFila_head . $contenidoNodo_0 . $imagen .  $contenidoNodo_1 . $titulo. $contenidoNodo_2 . $url . $contenidoNodo_3 ;
			
			$num_columnas_contador ++;
			
		} else{
			
			if(	$num_columnas_contador == $num_columnas ){
				// Si es el último de la fila INSERTO foot  e inicializo contador
				$contenido .= $contenidoNodo_0 . $imagen .  $contenidoNodo_1 . $titulo. $contenidoNodo_2 . $url . $contenidoNodo_3 . $contenidoFila_foot;
			
				$num_columnas_contador = 1;
			}else{
				
				// si es el nodo intermedio solo incremento contador
				 
				$contenido .= $contenidoNodo_0 . $imagen .  $contenidoNodo_1 . $titulo. $contenidoNodo_2 . $url . $contenidoNodo_3;
				
				$num_columnas_contador ++ ; 
			}
		}
		
	 
		
	} }else{
		$error= '<!-- sin conexion --> ';
	} 
	} // Fin FOR
	
	
	
	
	
	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-formacion-template-galeriav1-estilo7.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $contenido . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
	

	
return ($devuelvo  );
}


function auzalan_formacion_template_galeria_v1_estilo7p( $array, $atts) {
	$error= '';
	$version ='<!-- -------------------------------- < AUZALAN VERSION: ' . __AUZ_VERSION__ .' PLANTILLA: auzalan-formacion-template-galeriav1-estilo7  FUNCION: auzalan_formacion_template_galeria_v1_estilo7p > -->';
	$pie='<!-- --------------------------------< FIN AUZALAN - -->';
	
	$contenido='';
	
	for($i=0;$i<count($array);$i++){
		if (!empty($array[$i]['idPb'])) {		


			

		
			$imagen='https://aparejadoresalbacete.es/wp-content/uploads/2022/04/NOVEDADES-copia.png';
			$title=$array[$i]['titulo'];
			 
			 
			
			if ($array[$i]['imagen'] != Null){
				$imagen =$array[$i]['imagen'];
			}
			$url=__FORMACION_AUZALAN_POST_VIEWER_PAGE__ . '?postid='. $array[$i]['idPb'];
			
			
			 
			$contenido=$contenido."
				<li><div class='et_pb_blurb_content'>
								<div class='et_pb_main_blurb_image'><a href='" . $url . "'><span class='et_pb_image_wrap et_pb_only_image_mode_wrap has-box-shadow-overlay'><img src='" . $imagen . "' alt='' class='et-waypoint et_pb_animation_top et_pb_animation_top_tablet et_pb_animation_top_phone wp-image-1921 et-animated auzalan_mi_sombra' width='300' height='200'></span></a></div>
								<div class='et_pb_blurb_container'>
									<h5 class='et_pb_module_header'><a href='" . $url . "'>" . $title . "</a></h5>									
								</div>
							</div></li>
				";
			 
		}
	}
	
	
	
	
	
	
	$estilo = ' <link rel="stylesheet" href="'.__FORMACION_AUZALAN_POST_VIEWER_PAGE__.'css/auzalan-template-galeriav1-estilo7p.css" type="text/css"> ';
	
	if ($error == '' ){
		$devuelvo =$version . $estilo . "<ul class='auz_gallery_v1'>" .  $contenido . "</ul>" .  $pie ;
	}else	{
		$devuelvo =$error;
	}
	
	
	
	
return ($devuelvo);
}