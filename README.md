# PLUGIN PARA WORDPRESS
#### AUZALAN  

* Version:  2022.04.08


* Web de Auzalan: http://www.auzalan.net

* REQUISITO IMAGENES:
proporcion de imagenes: 6x4 ejemplo: 600px X 400px  o  300px X 200px
----------------------------------------

#### CONFIGURACI�N
##### PARMETROS CONFIGURABLES DEL PLUGIN
#####  define(�CLAVE_USUARIO�,���..�);
#####  define(�AUZALAN_URL�,���..�);
#####  define(�FORMACION_AUZALAN_POST_VIEWER_PAGE�,AUZALAN_URL.���../�);
#####  define(�AUZALAN_FORMACION_TEXTO_NoResultados�,�En estos momentos no hay actividades en periodo de inscripcin.�);

 
#### DESCRIPCI�N

#####  CLAVE_USUARIO: Solicitar

#####  AUZALAN_URL: Direcci�n de la ubicaci�n de su plugin, ejemplo (https://susitioweb.es/)

#####  FORMACION_AUZALAN_POST_VIEWER_PAGE: Url a una p�gina que se ha de crear para ampliar informaci�n a un determinado post de Auzalan. ejemplo: AUZALAN_URL.�detalle_formacionauzalan�

#####  AUZALAN_FORMACION_TEXTO_NoResultados: Texto descriptivo si no existen actividades, cursos, noticias relaccionadas�

________________________________________

AUZALAN CMS
ESTILOS APLICABLES

NECESARIO CREAR PGINA O POST CON ESTE CDIGO para FORMACI�N
#### plg_auzalan_post_viewer_form]
#### plg_auzalan_post_viewer_form estilo=�estilo7']

Ejemplos de uso FORMACION auzalan

##### plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo1']
##### plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo2']
##### plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo3']
##### plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo4']
##### plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo5']

##### CURSOS PROPIOS: en una sola columna y se abre en p�gina nueva. (DE SU COLEGIO) plg_auzalan_galeria bdd=�formacion� estilo=�estilo5' num_columnas=�1' colegio=�2' ]
##### CURSOS EXTERNOS: en una sola columna y se abre en p�gina nueva (EXCEPTO SU COLEGIO) plg_auzalan_galeria bdd=�formacion� estilo=�estilo5' num_columnas=�1' colegio_excluye=�2' ]
________________________________________
Estilos ver ejemplos: 
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo1/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo2/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo3/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo4/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo5/

________________________________________
## VERSIONES
##### 2022.04.08 ( Arreglar estilo7 auzalan alguna clase interfer�a con las plantillas DIVI)
##### 2022.04.07 ( ORDEN por FECHA LIMITE INSCRIPCION ) Nuevo comando en el plugin [    orden="ASC"  orden="DESC"  ] 
##### 2022.04.07 ( Reparado estilo 7p  ) link no llevaba al detalle del curso.
